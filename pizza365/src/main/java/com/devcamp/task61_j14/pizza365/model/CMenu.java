package com.devcamp.task61_j14.pizza365.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "menu")
public class CMenu {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO) 
    private Long id;
    
    @Column(name = "ten_san_pham")
    private String tenSanPham;
    
    @Column(name = "ma_san_pham")
    private String maSanPham;
    
    @Column(name = "gia_tien")
    private Long giaTien;
    
    @Column(name = "ngay_tao")
    private Long ngayTao;
    
    @Column(name = "ngay_cap_nhat")
    private Long ngayCapNhat;
    
    public CMenu() {
    }

    public CMenu(Long id, String tenSanPham, String maSanPham, Long giaTien, Long ngayTao, Long ngayCapNhat) {
        this.id = id;
        this.tenSanPham = tenSanPham;
        this.maSanPham = maSanPham;
        this.giaTien = giaTien;
        this.ngayTao = ngayTao;
        this.ngayCapNhat = ngayCapNhat;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTenSanPham() {
        return tenSanPham;
    }

    public void setTenSanPham(String tenSanPham) {
        this.tenSanPham = tenSanPham;
    }

    public String getMaSanPham() {
        return maSanPham;
    }

    public void setMaSanPham(String maSanPham) {
        this.maSanPham = maSanPham;
    }

    public Long getGiaTien() {
        return giaTien;
    }

    public void setGiaTien(Long giaTien) {
        this.giaTien = giaTien;
    }

    public Long getNgayTao() {
        return ngayTao;
    }

    public void setNgayTao(Long ngayTao) {
        this.ngayTao = ngayTao;
    }

    public Long getNgayCapNhat() {
        return ngayCapNhat;
    }

    public void setNgayCapNhat(Long ngayCapNhat) {
        this.ngayCapNhat = ngayCapNhat;
    }

    
}
    
