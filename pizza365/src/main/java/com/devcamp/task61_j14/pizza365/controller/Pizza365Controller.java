package com.devcamp.task61_j14.pizza365.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.task61_j14.pizza365.model.CCustomer;
import com.devcamp.task61_j14.pizza365.model.CDrink;
import com.devcamp.task61_j14.pizza365.model.CMenu;
import com.devcamp.task61_j14.pizza365.model.COrder;
import com.devcamp.task61_j14.pizza365.model.CVoucher;
import com.devcamp.task61_j14.pizza365.repository.CustomerRepository;
import com.devcamp.task61_j14.pizza365.repository.DrinkRepository;
import com.devcamp.task61_j14.pizza365.repository.MenuRepository;
import com.devcamp.task61_j14.pizza365.repository.OrderRepository;
import com.devcamp.task61_j14.pizza365.repository.VoucherRepository;

@RequestMapping("/")
@CrossOrigin
@RestController
public class Pizza365Controller {

    @Autowired
    VoucherRepository voucherRepository;

    @GetMapping("/vouchers")
    public ResponseEntity<List<CVoucher>> getVouchers(){
        
        try {
            List<CVoucher> listVoucher = new ArrayList<CVoucher>();
          /*   voucherRepository.findAll().forEach((ele)->{
                listVoucher.add(ele);
            });
          */
            voucherRepository.findAll().forEach(listVoucher::add);
            if (listVoucher.size() == 0){
                return new ResponseEntity<>(listVoucher, HttpStatus.NOT_FOUND);
            }else{
                return new ResponseEntity<>(listVoucher, HttpStatus.OK);
            }
        }
        catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    @Autowired
    DrinkRepository drinkRespo;
   

    @GetMapping("/drinks")
    public ResponseEntity<List<CDrink>> getAllDrinks(){
        try {
            List<CDrink> listCustomer = new ArrayList<CDrink>();
            
            drinkRespo.findAll().forEach(listCustomer::add);
            return new ResponseEntity<>(listCustomer, HttpStatus.OK);


        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Autowired
    CustomerRepository pCustomerRepository;

    @GetMapping("/customers")
    public ResponseEntity<List<CCustomer>> getAllCustomers(){
        try {
            List<CCustomer> listCustomer = new ArrayList<CCustomer>();
            pCustomerRepository.findAll().forEach(listCustomer::add);
            return new ResponseEntity<>(listCustomer, HttpStatus.OK);

        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Autowired
    OrderRepository orderRepository;

    @GetMapping("/orders")
    public ResponseEntity<List<COrder>> getAllOrders(){
        try {
            List<COrder> listOrder  = new ArrayList<COrder>();
            orderRepository.findAll().forEach(listOrder::add);
            return new ResponseEntity<>(listOrder, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Autowired
    MenuRepository productRepository;

    @GetMapping("/menu")
    public ResponseEntity<List<CMenu>> getAllProducts(){
        try {
            List<CMenu> listProduct = new ArrayList<CMenu>();
            productRepository.findAll().forEach(listProduct::add);
            return new ResponseEntity<>(listProduct, HttpStatus.OK);

        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
