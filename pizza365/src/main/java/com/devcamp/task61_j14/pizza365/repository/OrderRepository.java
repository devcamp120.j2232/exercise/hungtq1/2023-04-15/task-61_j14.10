package com.devcamp.task61_j14.pizza365.repository;

import org.springframework.data.repository.Repository;

import com.devcamp.task61_j14.pizza365.model.COrder;

public interface OrderRepository extends Repository<COrder, Long> {

    Iterable<COrder> findAll();
    
}
