package com.devcamp.task61_j14.pizza365.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.task61_j14.pizza365.model.CDrink;

public interface DrinkRepository extends JpaRepository<CDrink, Long>{
    
}
