package com.devcamp.task61_j14.pizza365.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.task61_j14.pizza365.model.CCustomer;

public interface CustomerRepository extends JpaRepository<CCustomer, Long>{
    
}
